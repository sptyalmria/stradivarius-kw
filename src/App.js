import React from 'react';
import './App.css';
import {
  Navbar,
  Dropdown,
  Container,
  Form,
  Nav,
  FormControl,
  Button,
  Carousel,
  Image,
  Row,
  Col
} from 'react-bootstrap';


function Header(){
  return(
      <Navbar bg="light" expand="lg" fixed="top">
           <Dropdown>
            <Dropdown.Toggle variant="light" id="dropdown-basic">
              <svg style={{ width:"19px", height:"18px" }} viewBox="0 0 19 18" version="1.1" xmlns="http://www.w3.org/2000/svg">
                 <g id="D01_IconBurgerMenu" transform="translate(-53.000000, -15.000000)">
                    <g id="Header-Filtros_IconBurgerMenu">
                       <g id="menu_IconBurgerMenu" transform="translate(53.000000, 15.000000)">
                        <path d="M18,16.25 C18.4142136,16.25 18.75,16.5857864 18.75,17 C18.75,17.4142136 18.4142136,17.75 18,17.75 L18,17.75 L1,17.75 C0.585786438,17.75 0.25,17.4142136 0.25,17 C0.25,16.5857864 0.585786438,16.25 1,16.25 L1,16.25 Z M12,8.25 C12.4142136,8.25 12.75,8.58578644 12.75,9 C12.75,9.41421356 12.4142136,9.75 12,9.75 L12,9.75 L1,9.75 C0.585786438,9.75 0.25,9.41421356 0.25,9 C0.25,8.58578644 0.585786438,8.25 1,8.25 L1,8.25 Z M18,0.25 C18.4142136,0.25 18.75,0.585786438 18.75,1 C18.75,1.41421356 18.4142136,1.75 18,1.75 L18,1.75 L1,1.75 C0.585786438,1.75 0.25,1.41421356 0.25,1 C0.25,0.585786438 0.585786438,0.25 1,0.25 L1,0.25 Z" id="burger_IconBurgerMenu"></path>
                       </g>
                   </g>
                  </g>
              </svg>
            </Dropdown.Toggle>
            <Dropdown.Menu>
              <Dropdown.Item href="#/action-1">JUST IN</Dropdown.Item>
              <Dropdown.Item href="#/action-2">CLOTHING</Dropdown.Item>
              <Dropdown.Item href="#/action-3">SHOES</Dropdown.Item>
              <Dropdown.Item href="#/action-3">ACCESSORIES</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
             <Image src="https://static.e-stradivarius.net/5/static2/itxwebstandard/images/logos/logo_stradivarius.svg?20210111023001" height="40" width="130" style={{marginLeft:"700px"}}/>
            </Nav>
            <Form inline>
              <FormControl type="text" placeholder="Search by product, collection…" className="mr-sm-2" />
              <Button variant="outline-secondary">
                <svg width="15px" height="14px" viewBox="0 0 15 14" version="1.1" xmlns="http://www.w3.org/2000/svg">
                    <g transform="translate(-990.000000, -16.000000)">
                      <g>
                        <g transform="translate(988.000000, 13.000000)">
                          <g transform="translate(10.500000, 10.500000) rotate(-45.000000) translate(-10.500000, -10.500000) translate(5.000000, 2.000000)">
                            <path d="M5.24999991,0 C8.1494948,0 10.5,2.35050502 10.5,5.24999991 C10.5,8.06345838 8.28691782,10.360024 5.50661196,10.4938383 L5.77446112,16.4012011 C5.78759378,16.690853 5.56343062,16.9363084 5.2737787,16.949441 C4.98412677,16.9625737 4.73867136,16.7384106 4.7255387,16.4487586 L4.7255387,16.4487586 L4.45222306,10.4397747 C1.93133598,10.0554608 -1.81987758e-12,7.8782741 -1.81987758e-12,5.24999991 C-1.81987758e-12,2.35050502 2.35050502,0 5.24999991,0 Z M5.24999991,1.04999998 C2.930404,1.04999998 1.04999998,2.930404 1.04999998,5.24999991 C1.04999998,7.56959587 2.93040405,9.45000002 5.24999991,9.45000002 C7.56959582,9.45000002 9.45000002,7.56959582 9.45000002,5.24999991 C9.45000002,2.93040405 7.56959587,1.04999998 5.24999991,1.04999998 Z"></path>
                          </g>
                        </g>
                      </g>
                    </g>
                </svg>
              </Button>
            </Form>
          </Navbar.Collapse>
      </Navbar>
    );
}

function Banner(){
  return(
<Carousel>
  <Carousel.Item style={{ maxHeight: "800px" }}>
    <img
      className="d-block w-100"
      src="https://static.e-stradivarius.net/5/static2/homes/2020_rebajas_OI_1/img/mkt-w/1920/no-venta/2400_rebajas.jpg?t=20210111023002"
      alt="First slide"
    />
    <Carousel.Caption>
        <div style={{ marginBottom: "140px", textAlign:"center" }}>
          <h2 style={{ fontSize:"200px", fontWeight:"bold", color:"red" }}>SALE</h2>
          <h3 style={{ color:"red", fontSize:"30px", }}>Discover the promotion at your nearest store!</h3>
          <h3 style={{ color:"red", fontSize:"30px", marginBottom: "60px" }}>Buy using our telephone sales service</h3>
          <a href="/"  style={{ color:"red", fontSize:"20px" }}>DISCOVER THE PROMOTION</a>
        </div>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item style={{ maxHeight: "800px" }}>
    <img
      className="d-block w-100"
      src="https://static.e-stradivarius.net/5/static2/homes/2020_rebajas_OI_1/img/mkt-w/1920/2400_newin_w2.jpg?t=20210113165432"
      alt="Third slide"
    />

    <Carousel.Caption>
        <div style={{ marginBottom: "150px", textAlign:"center" }}>
          <h2 style={{ fontSize:"200px", fontWeight:"bold", color:"white", marginBottom: "70px" }}>NEW IN</h2>
          <a href="/"  style={{ color:"white", fontSize:"20px" }}>VIEW ALL</a>
        </div>
    </Carousel.Caption>
  </Carousel.Item>
</Carousel>
    );
}

function Main(){
   const Fitur = (props) =>{
    return(
        <Image src={props.gambar} fluid />
      );
   }

    return(
        <div>
          <Fitur
            gambar="https://static.e-stradivarius.net/5/static2/homes/2020_rebajas_OI_1/img/mkt-w/1920/Venta_telefonica/2400_1_ID_en.jpg?t=20210111023002"
          />
          <Fitur
            gambar="https://static.e-stradivarius.net/5/static2/homes/2020_rebajas_OI_1/img/mkt-w/1920/Venta_telefonica/2400_2_ID_en.jpg?t=20210111023002"
          />
        </div>
      );
}

function Parag(){
  return(
    <div style={{ textAlign:"center",  marginTop: "10px", borderBottom: "1px solid rgb(233, 233, 233)", padding: "30px 20px" }}>
      <p style={{ color:"#8E8E8E", fontSize:"14px" }}>Stradivarius online women's clothing store</p>
    </div>
  );
}

function Footer(){
  return(
    <div>
      <Row style={{ marginRight: "600px", marginLeft: "600px" }}>
        <Col>
        <Form>
            <Form.Group controlId="formBasicEmail">
            <div style={{ textAlign:"center", fontSize:"25px", fontFamily:"neue-haas-grotesk-text", marginTop: "40px"}}>
              <Form.Label>SIGN UP FOR OUR NEWSLETTER</Form.Label>
            </div>
              <Form.Control type="email" placeholder="Email Address" />
              <div style={{ fontStyle:"italic", color:"#8E8E8E", fontSize:"12px", textAlign:"left" }}>
              <Form.Group controlId="formBasicCheckbox"><br/>
                <Form.Check type="checkbox" label=" I have been able to read and understand the information on the use of my personal data explained in the Privacy Policy and agree to receive customised commercial communications from Stradivarius via email and other means" />
              </Form.Group>
              </div>
            </Form.Group>
             <Button variant="secondary" size="lg" block>
              SIGN UP
            </Button>
          </Form>
          </Col>
      </Row>
      <Row >
          <Col style={{ marginLeft:"450px", marginTop: "40px"}}>
            <ul>
              <h6><b>Dou you need help?</b></h6>
              <li style={{ listStyleType:"none" }}><a href="/" style={{ color:"#8E8E8E" }}>Contact</a></li>
              <li style={{ listStyleType:"none" }}><a href="/" style={{ color:"#8E8E8E" }}>Cookie Preferences</a></li>
            </ul>
          </Col>
          <Col style={{ marginLeft:"250px",  marginRight:"350px", marginTop: "40px"}}> 
            <ul>
                <h6><b>Us</b></h6>
                <li style={{ listStyleType:"none" }}><a href="/" style={{ color:"#8E8E8E" }}>Stores</a></li>
                <li style={{ listStyleType:"none" }}><a href="/" style={{ color:"#8E8E8E" }}>Company</a></li>
                <li style={{ listStyleType:"none" }}><a href="/" style={{ color:"#8E8E8E" }}>Work with us</a></li>
                <li style={{ listStyleType:"none" }}><a href="/" style={{ color:"#8E8E8E" }}>Press room</a></li>
            </ul>
          </Col>
      </Row>
    </div>
  );
}

function Footer2(){
  return(
  <div style={{ borderTop: "1px solid rgb(233, 233, 233)", borderBottom: "1px solid rgb(233, 233, 233)", textAlign: "center", fontSize: "15px" }}>
    <div className="row">
      <div className="col-md-3" style={{ borderRight: "1px solid rgb(233, 233, 233)", padding: "10px 0px", marginRight: "-15px", marginLeft: "-15px" }}>
        <ul style={{ textAlign: "center" }}>
          <li style={{listStyleType:"none"}}>
            <a href="/" style={{ marginRight: "10px" }}>
              <svg className="svg-drawing social-icon" viewBox="0 0 23 48" style={{ width: "20px", height: "20px"}}>
                <path d="M4.94856582,48 L14.8537746,48 L14.8537746,23.9973043 L21.4608369,23.9973043 L22.3358553,15.7268337 L14.8537746,15.7268337 L14.864544,11.5862069 C14.864544,9.42963046 15.0691637,8.27316635 18.1599981,8.27316635 L22.2900851,8.27316635 L22.2900851,0 L15.6803304,0 C7.74054774,0 4.94856582,4.01392789 4.94856582,10.7613164 L4.94856582,15.7268337 L0,15.7268337 L0,24 L4.94856582,24 L4.94856582,48 L4.94856582,48 Z"></path>
              </svg>
            </a>

            <a href="/" style={{ marginRight: "10px", marginLeft: "10px" }}>
              <svg className="svg-drawing icon-instagram" viewBox="0 0 24 24" style={{ width: "24px", height: "24px" }}>
                <path d="M15.5,3h-7A5.5,5.5,0,0,0,3,8.5v7A5.5,5.5,0,0,0,8.5,21h7A5.5,5.5,0,0,0,21,15.5v-7A5.5,5.5,0,0,0,15.5,3Zm4,12.5a4.00454,4.00454,0,0,1-4,4h-7a4.00454,4.00454,0,0,1-4-4v-7a4.0045,4.0045,0,0,1,4-4h7a4.0045,4.0045,0,0,1,4,4Z"></path>
                <path d="M12,7.5A4.5,4.5,0,1,0,16.5,12,4.5,4.5,0,0,0,12,7.5ZM12,15a3,3,0,1,1,3-3A3.0034,3.0034,0,0,1,12,15Z"></path>
                <circle cx="16.70001" cy="7.29999" r="1"></circle>
              </svg>
            </a>

            <a href="/" style={{ marginRight: "10px", marginLeft: "10px" }}>
              <svg className="svg-drawing social-icon" viewBox="0 0 47 39" style={{ width: "20px", height: "20px" }}>
                <path d="M29.4326653,0.45958164 C25.1898785,2.00464664 22.5081171,5.98605767 22.8123169,10.343728 L22.9137168,12.0248869 L21.2166021,11.8194119 C15.0392114,11.0295341 9.64099906,8.35302253 5.05665464,3.85658984 L2.8151824,1.62571878 L2.24147224,3.27218702 C1.01933619,6.94405134 1.80118305,10.8240591 4.3468551,13.433858 C5.70508054,14.8748513 5.39821231,15.0803263 3.05800856,14.2237358 C2.24414066,13.9488797 1.52900428,13.7434047 1.4622938,13.8474765 C1.22480447,14.087642 2.03867237,17.2097941 2.68442985,18.4479809 C3.56767665,20.1664988 5.36619128,21.8449891 7.33548475,22.8430104 L9.00057842,23.6328882 L7.02861653,23.6622417 C5.12870196,23.6622417 5.05932306,23.6969323 5.26479135,24.4200974 C5.94256986,26.6509685 8.62433129,29.0206019 11.6129609,30.0506452 L13.7183438,30.7711418 L11.8851397,31.8705663 C9.16868883,33.4529904 5.97725931,34.3442714 2.78582979,34.4083156 C1.25682551,34.4430062 0,34.5791 0,34.6831717 C0,35.0247405 4.14405523,36.9460648 6.55096947,37.7039206 C13.7823859,39.9347916 22.3720277,38.9714609 28.8215972,35.1635028 C33.4059416,32.4523007 37.9876176,27.0619184 40.12769,21.8423206 C41.2831155,19.0644058 42.4358727,13.9835703 42.4358727,11.5472243 C42.4358727,9.96746869 42.5372726,9.76199372 44.4371872,7.87535995 C45.5579233,6.77593546 46.6092805,5.57510773 46.8147488,5.23353896 C47.1563065,4.57975498 47.1189486,4.57975498 45.3898129,5.1641578 C42.5052516,6.19420114 42.0969834,6.05810733 43.5245878,4.51304233 C44.575945,3.41361784 45.8327705,1.42291232 45.8327705,0.841178005 C45.8327705,0.739774776 45.3231024,0.910559163 44.7467238,1.21743736 C44.1356558,1.56167464 42.7774304,2.07669631 41.7580942,2.3835745 L39.9248901,2.96797733 L38.2624648,1.83119375 C37.3445286,1.21476885 36.0583505,0.526294292 35.3779035,0.320819326 C33.643431,-0.159511763 30.996359,-0.0901306054 29.4326653,0.45958164 L29.4326653,0.45958164 Z"></path>
              </svg>
            </a>
            <a href="/" style={{ marginRight: "10px", marginLeft: "10px" }}>
              <svg className="svg-drawing icon-youtube" viewBox="0 0 24 24" style={{ width: "24px", height: "24px" }}>
                <path d="M21.80731,7.805a2.79915,2.79915,0,0,0-2.54907-2.53906C17.69061,5.13586,15.36707,5,12.50012,5c-3.45251,0-6.05865.14105-7.74353.27332A2.79932,2.79932,0,0,0,2.1925,7.815C2.08936,8.90021,2,10.33344,2,12.0423c0,1.66632.08862,3.07422.19128,4.14594a2.79852,2.79852,0,0,0,2.56384,2.53833C6.43994,18.85883,9.04657,19,12.50012,19c2.86694,0,5.19049-.13586,6.75812-.266a2.799,2.799,0,0,0,2.54907-2.539C21.91052,15.11548,22,13.69189,22,12S21.91052,8.88452,21.80731,7.805ZM9.77594,14.93878V9.06122L15.574,12Z"></path>
              </svg>
            </a>

            <a href="/" style={{ marginLeft: "10px" }}>
              <svg className="svg-drawing social-icon" viewBox="0 0 45 45" style={{ width: "20px", height: "20px" }}>
                <path d="M22.500738,0 C10.0746954,0 0,10.0737114 0,22.50041 C0,31.7137484 5.54027065,39.628655 13.4681337,43.1086883 C13.4048276,41.5375154 13.4568173,39.6514518 13.8597789,37.9420222 C14.2922615,36.1153287 16.7549685,25.6816252 16.7549685,25.6816252 C16.7549685,25.6816252 16.0359682,24.2451008 16.0359682,22.1215573 C16.0359682,18.7873249 17.9687733,16.297065 20.3753904,16.297065 C22.4220154,16.297065 23.4108047,17.8342888 23.4108047,19.6749228 C23.4108047,21.7323721 22.0985965,24.8097718 21.4237137,27.6600238 C20.8600268,30.0467962 22.6204621,31.9935418 24.9750894,31.9935418 C29.238085,31.9935418 32.1093297,26.5182174 32.1093297,20.0308148 C32.1093297,15.0994967 28.7878898,11.4083883 22.7469103,11.4083883 C15.9216564,11.4083883 11.6696491,16.4982998 11.6696491,22.1837153 C11.6696491,24.1439094 12.2476046,25.526476 13.1529151,26.596776 C13.5691611,27.0884646 13.6270551,27.2864192 13.476334,27.8509263 C13.3684184,28.2648762 13.120606,29.2615378 13.0179386,29.6566271 C12.8680375,30.2263823 12.406362,30.4300772 11.8913846,30.2196581 C8.7477267,28.9363148 7.28348537,25.4936749 7.28348537,21.6236365 C7.28348537,15.2321772 12.674019,7.56803496 23.3643911,7.56803496 C31.9548365,7.56803496 37.608763,13.7843364 37.608763,20.4572292 C37.608763,29.2836785 32.7017177,35.8778487 25.468418,35.8778487 C23.0391682,35.8778487 20.7540792,34.5646564 19.9714448,33.0733542 C19.9714448,33.0733542 18.6651408,38.2574048 18.3884635,39.2584946 C17.9115354,40.993345 16.977688,42.7270474 16.1238752,44.078453 C18.1475394,44.6759251 20.2851874,45.001148 22.500738,45.001148 C34.9257966,45.001148 45,34.9272727 45,22.50041 C45,10.0737114 34.9257966,0 22.500738,0"></path>
              </svg>
            </a>
          </li>
        </ul>
      </div>

      <div className="col-md-3" style={{ borderRight: "1px solid rgb(233, 233, 233)", padding: "10px 0px" }}>
        <ul style={{ textAlign: "center" }}>
          <li style={{ listStyleType:"none" }}>
            <a href="/">
              <span className="app-link-icon" style={{ marginRight: "10px" }}>
                <svg className="svg-drawing icon-apple" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 170 170" version="1.1" style={{ width: "20px", height: "20px" }}>
                  <path d="m150.37 130.25c-2.45 5.66-5.35 10.87-8.71 15.66-4.58 6.53-8.33 11.05-11.22 13.56-4.48 4.12-9.28 6.23-14.42 6.35-3.69 0-8.14-1.05-13.32-3.18-5.197-2.12-9.973-3.17-14.34-3.17-4.58 0-9.492 1.05-14.746 3.17-5.262 2.13-9.501 3.24-12.742 3.35-4.929 0.21-9.842-1.96-14.746-6.52-3.13-2.73-7.045-7.41-11.735-14.04-5.032-7.08-9.169-15.29-12.41-24.65-3.471-10.11-5.211-19.9-5.211-29.378 0-10.857 2.346-20.221 7.045-28.068 3.693-6.303 8.606-11.275 14.755-14.925s12.793-5.51 19.948-5.629c3.915 0 9.049 1.211 15.429 3.591 6.362 2.388 10.447 3.599 12.238 3.599 1.339 0 5.877-1.416 13.57-4.239 7.275-2.618 13.415-3.702 18.445-3.275 13.63 1.1 23.87 6.473 30.68 16.153-12.19 7.386-18.22 17.731-18.1 31.002 0.11 10.337 3.86 18.939 11.23 25.769 3.34 3.17 7.07 5.62 11.22 7.36-0.9 2.61-1.85 5.11-2.86 7.51zm-31.26-123.01c0 8.1021-2.96 15.667-8.86 22.669-7.12 8.324-15.732 13.134-25.071 12.375-0.119-0.972-0.188-1.995-0.188-3.07 0-7.778 3.386-16.102 9.399-22.908 3.002-3.446 6.82-6.3113 11.45-8.597 4.62-2.2516 8.99-3.4968 13.1-3.71 0.12 1.0831 0.17 2.1663 0.17 3.2409z"></path>
                </svg>
              </span>
              <span className="app-link-text" style={{ fontWeight: "bold", color:"#4A4A4A" }}>app iOS</span>
            </a>
          </li>
        </ul>
      </div>

      <div className="col-md-3" style={{ borderRight: "1px solid rgb(233, 233, 233)", padding: "10px 0px" }}>
        <ul style={{ textAlign: "center" }}>
          <li style={{ listStyleType:"none" }}>
            <a href="/">
              <span className="app-link-icon" style={{ marginRight: "10px", marginLeft: "10px" }}>
                <svg className="svg-drawing icon-android" viewBox="0 0 24 24" style={{ width: "24px", height: "24px" }}>
                  <path d="M0 0h24v24H0z" fill="none"></path><path d="M6 18c0 .55.45 1 1 1h1v3.5c0 .83.67 1.5 1.5 1.5s1.5-.67 1.5-1.5V19h2v3.5c0 .83.67 1.5 1.5 1.5s1.5-.67 1.5-1.5V19h1c.55 0 1-.45 1-1V8H6v10zM3.5 8C2.67 8 2 8.67 2 9.5v7c0 .83.67 1.5 1.5 1.5S5 17.33 5 16.5v-7C5 8.67 4.33 8 3.5 8zm17 0c-.83 0-1.5.67-1.5 1.5v7c0 .83.67 1.5 1.5 1.5s1.5-.67 1.5-1.5v-7c0-.83-.67-1.5-1.5-1.5zm-4.97-5.84l1.3-1.3c.2-.2.2-.51 0-.71-.2-.2-.51-.2-.71 0l-1.48 1.48C13.85 1.23 12.95 1 12 1c-.96 0-1.86.23-2.66.63L7.85.15c-.2-.2-.51-.2-.71 0-.2.2-.2.51 0 .71l1.31 1.31C6.97 3.26 6 5.01 6 7h12c0-1.99-.97-3.75-2.47-4.84zM10 5H9V4h1v1zm5 0h-1V4h1v1z"></path>
                </svg>
              </span>
              <span className="app-link-text" style={{ fontWeight: "bold", color:"#4A4A4A" }}>app Android</span>
            </a>
          </li>
        </ul>
      </div>

      <div className="col-md-3" style={{ padding: "10px 0px" }}>
        <ul style={{ textAlign: "center" }}>
          <li style={{ listStyleType:"none" }}>
            <a href="https://www.stradivarius.com?select-store=true" style={{ marginRight: "10px", marginLeft: "10px", textDecoration: "underline", fontWeight: "bold", color:"#4A4A4A"}}>Indonesia</a>
              <span>|</span>
              <a href="/" style={{ marginRight: "10px", marginLeft: "10px", textDecoration: "underline", fontWeight: "bold", color:"#4A4A4A" }}> English
              <span className="localization-links-arrow">
                <svg className="icon-default-arrow svg-drawing" viewBox="0 0 129 129" version="1.1" style={{ width: "10px", height: "10px", marginLeft: "10px" }}>
                  <title></title>
                  <path d="m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z"></path>
                </svg>
              </span>
            </a>
          </li>
        </ul>
      </div>

    </div>
  </div>
);
}

function Footer3() {
  return (
    <div style={{ textAlign:"center", marginTop:"30px", marginBottom: "30px" }}>
      <div style={{ color:"#8E8E8E", fontSize:"13px" }}>
      © 2021 Stradivarius
      <a href="/" style={{ color:"#8E8E8E" }}> · Legal notice</a>
      <a href="/" style={{ color:"#8E8E8E" }}> · Cookies</a>
      <a href="/" style={{ color:"#8E8E8E" }}> · Privacy policy</a>
      <a href="/" style={{ color:"#8E8E8E" }}> · Unsubscribe from newsletter</a>
      </div>
    </div>
  );
}

function App() {
  return (
    <div>
      <Header/>
      <Banner/>
      <Main/>
      <Parag/>
      <Footer/>
      <Footer2/>
      <Footer3/>
    </div>    
  );
}

export default App;
